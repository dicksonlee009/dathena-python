FROM python:2.7-slim

WORKDIR /app

COPY . /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 90

ENV FLASK_APP run.py

CMD ["flask","run","--host=0.0.0.0","--port=90"]

